$(function() {
    "use strict";
    var data = [
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0]
    ];

    var model = {
        init: function() {
            //console.log('model init');
			// Inicializamos el tablero
			
			for(var i=0;i<10;i++){
				for(var j=0;j<10;j++){
					// Generamos un random
					var random = Math.floor(Math.random() * 4); // Del 0 al 4
					
					// Si es 1, será mina, si no, zona segura
					if(random==1){
						data[i][j] = -9;
					}else{
						data[i][j] = -8;
					}
				}
			}
			
			//console.log("Tablero después de iniciar:"+data);

        },
		contarMinas: function(x,y){
			
			var numeroMinas = 0;
			
			for(var i=x-1;i<=x+1;i++){
				for(var j=y-1;j<=y+1;j++){
					// Antes que nada, es importante limitar el tablero
					if((i>=0&&j>=0)&&(i<10&&j<10)){
						//console.log("contarMinas antes de empezar: "+i+","+j);
						// No contamos su misma posición
						if( i==x && j==y ){
							//console.log("NO CONTAMOS LA MISMA POSICIÓN, NO NO NO: "+i+","+j);
						}else{
							// Si es -9 la posición, hay mina
							if(controller.getPos(i,j)==-9){
								numeroMinas++;
							}
							//console.log("Contamos minas mina en posición: "+i+","+j);
						}
						
					}
				}
			}
			return numeroMinas;
		}
    };

    var controller = {
        //Al arrancar, lo que hacemos es iniciar el modelo y inicializar la vista
        init: function() {
			//console.log('controller init');
            model.init();
            view.init();
        },
		// Nos obtiene el valor lógico de la casilla 
		// -9 MINA NO VISTA --> Icono GRIS
		// -8 NO MINA NO VISTA --> Icono GRIS
		// -5 MINA VISITADA --> EXPLOSION 
		// 0..8 NO MINA VISITADA --> Icono numero 0..8
		// -7 MINA MARCADA  --> Icono banderita
		// -6 NO MINA MARCADA --> Icono banderita
		getPos: function(i,j){
			//console.log('getPos:'+data[i][j]);
			return data[i][j];

		},
		// Función para obtener una copia del tablera - no está en uso
		getTablero: function(){
			return data;
		},
		// Función para guardar un cambio en el modelo
		setPos: function(x,y,minas){
			data[x][y] = minas;
		},
		// Función para descubrir el tablero
		descubrirTablero: function(){
			for(var i=0;i<10;i++){
				for(var j=0;j<10;j++){
					if(data[i][j]==-8){
						data[i][j]=0;
					}else if(data[i][j]==-9){
						data[i][j]=-5;
					}			
				}
			}
		},
		// Función para expandir el tablero
		expandir: function(x,y,expansion){
			// Si la expansion (recursividad) es la última, mostramos el número de minas
			if(expansion==1){
				data[x][y]=model.contarMinas(x,y);
			// En caso contrario, no sirve para nada contar las minas, marcamos la casilla y ya está
			}else{
				data[x][y]=0;
			}
			
			// Mientras sea mayor que 0, seguirá la recursividad
			if(expansion>0){
				
				// Realizamos la lógica para ir expandiendo hacia cada lado si toca
				if(x-1>=0) if (data[x-1][y]==-8) controller.expandir(x-1,y,expansion-1);
				if(y-1>=0) if (data[x][y-1]==-8) controller.expandir(x,y-1,expansion-1);
				if(y+1<10) if (data[x][y+1]==-8) controller.expandir(x,y+1,expansion-1);
				if(x+1<10) if (data[x+1][y]==-8) controller.expandir(x+1,y,expansion-1);
			}
		}
    };


    var view = {
        init: function() {
			//console.log('view init');
     		view.renderTablero();        
        },
        renderTablero : function(){
			var html='';
			html+='<table border="0">';
			var num=0;
			for(var i=0;i<10;i++){
				html+='<tr>';
				for(var j=0;j<10;j++){
					var posicion=controller.getPos(i,j);
					
					let imagen;
					// Realizmos todos los casos posibles según la lógica del enunciado expuesto
					switch(posicion){
							case -9: imagen="<img src='img/square.gif'>";
							break;
							case -8: imagen="<img src='img/square.gif'>";
							break;
							case -5: imagen="<img src='img/mina.png'>";
							break;
							case 0: imagen="<img src='img/Minesweeper_0.gif'>";
							break;
							case 1: imagen="<img src='img/Minesweeper_1.gif'>";
							break;
							case 2: imagen="<img src='img/Minesweeper_2.gif'>";
							break;
							case 3: imagen="<img src='img/Minesweeper_3.gif'>";
							break;
							case 4: imagen="<img src='img/Minesweeper_4.gif'>";
							break;
					}
					html+='<td id='+num+'>'+imagen+'</td>';
					num++;
				}
				html+='</tr>';
			}
			html+='</table>';
			
			$("#buscaminas").html(html);
			$("td").click(view.visitar);
        },
		visitar: function(){
			
			// Obtenemos la ID de la casilla clicada (0 al 99)
			var casilla = $(this).attr('id');
			//console.log("Casilla: "+casilla);
			
			// Realizamos el cálculo para obtener la posición del array bidimensional
			var x = parseInt(parseInt(casilla)/10);
			var y = parseInt(parseInt(casilla)%10);
			//console.log("X="+x+"|Y="+y);

			// Realizamos la lógica
			// Si la posición equivale a -9 ya no es necesario continuar, ha perdido
			if(controller.getPos(x,y)==-9){
				
				//console.log("HAS PERDIDO! vuelve a intentarlo");
				controller.descubrirTablero();
				alert("HAS PERDIDO");
				var html='<input type="button" value="Volver a empezar" onClick="window.location.reload()">';
				$("#menu").html(html);
				
			// En caso contrario, contamos las minas que hay alrededor para pintar la posición correcto después
			}else if(controller.getPos(x,y)==-8){
				
				// Llamamos al modelo para contar las minas alrededor
				var minasCercanas = model.contarMinas(x,y);
				
				// Llamamos al controlador para introducir el cambio en esa posición
				controller.setPos(x,y,minasCercanas);
				
				// Llamamos al controlador para expandir la posición y facilitar el juego
				controller.expandir(x,y,3);
				
			}
			// No hay ningún else if más, ya que con los demás click no queremos realizar lógica
			// Quiere decir que ya se ha clicado antes
	
			// Una vez actualizado, renderizamos de nuevo el tablero en la vista
			view.renderTablero();

		} 

    };

    controller.init();
});
