Proyecto: BUSCAMINAS Javascript Arquitectura MVC

#Descripción#

Se ha realizado la actividad siguiendo en lo máximo posible lo explicado en clase.

Se ha utilizado el esqueleto planteado en el documento y las recomendaciones vistas en las diferentes clases.

En ningún caso se ha utilizado material externo, ni se ha copiado contenido de ninguna proyecto externo. 

Tampoco es un proyecto reconvertido, todo lo utilizado se ha visto en clase y ha sido enseñado por Álvaro.

Autor: Eduard Vallès